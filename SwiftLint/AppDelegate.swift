//
//  AppDelegate.swift
//  SwiftLint
//
//  Created by Javier Fuentes on 4/11/20.
//  Copyright © 2020 Javier Fuentes. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

}
