//
//  ExampleViewController.swift
//  SwiftLint
//
//  Created by Javier Fuentes on 4/14/20.
//  Copyright © 2020 Javier Fuentes. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    var numberCases: Int = 0
    
    @available(*, unavailable, message: "This method was deprecated, to use a new method")
    func doSomething() {
        // Do something deprecated
        fatalError()
    }
    
}
