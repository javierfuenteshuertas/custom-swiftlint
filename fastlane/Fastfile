# ============================================================================ #
# Variables

default_platform :ios
project_dir = "SwiftLint.xcodeproj"
#project_workspace = "./Example/BCPCoreNetwork.xcworkspace"
project_scheme = "SwiftLint"
config_file_swiftlint = ".swiftlint.yml"
config_file_sonar_properties= "sonar-project.properties"

# ============================================================================ #

# ============================================================================ #
# Metrics lanes

lane :metrics_prepare do |options|
    sh("cd .. && rm -rf ./reports && mkdir -p ./reports")
end

lane :metrics_tests do |options|
    begin
        scan(
            #workspace: project_workspace,
            scheme: project_scheme,
            skip_build: true,
            code_coverage: true,
            output_directory: "./reports",
            output_types: "html,junit",
            output_files: "TEST-result.html,TEST-result-junit.xml",
            clean: true
        )
        rescue => ex
        puts ex
    end
end

lane :metrics_lint do |options|
    swiftlint(
        mode: :lint,
        output_file: "./reports/lint-result-junit.xml",
        # output_file: "./reports/lint-result.txt",
        config_file: config_file_swiftlint
    )
end

lane :metrics_coverage do |options|
    slather(       
        proj: project_dir, 
        workspace: project_workspace, 
        cobertura_xml: true,        
        scheme: project_scheme,
        output_directory: "./reports"
    )
end

lane :metrics_sonar do
    sonar(
        project_configuration_path: config_file_sonar_properties
    )
end

lane :metrics do |options|
    metrics_prepare
    #metrics_tests
    #metrics_coverage
    metrics_lint
    metrics_sonar
end

lane :ci_metrics do |options|
    metrics
end

# ============================================================================ #
# Utility lanes

lane :lint_autocorrect do |options|
    swiftlint(
        mode: :autocorrect,
        config_file: config_file_swiftlint
    )
end