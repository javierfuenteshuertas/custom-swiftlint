fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
### metrics_prepare
```
fastlane metrics_prepare
```

### metrics_tests
```
fastlane metrics_tests
```

### metrics_lint
```
fastlane metrics_lint
```

### metrics_coverage
```
fastlane metrics_coverage
```

### metrics_sonar
```
fastlane metrics_sonar
```

### metrics
```
fastlane metrics
```

### ci_metrics
```
fastlane ci_metrics
```

### lint_autocorrect
```
fastlane lint_autocorrect
```


----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
